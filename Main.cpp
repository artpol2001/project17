#include <iostream>
#include <math.h>

using namespace std;

class Calendar
{
public:
    Calendar() : Day(0), Month(0), Year(0)
    {}
    Calendar(int _Day, double _Month, double _Year) : Day(_Day), Month(_Month), Year(_Year)
    {}
    void Show()
    {
        cout << " Date Now : " << '0' << Day << '.' << '0' << Month << '.' << Year;

    }

private:
    int Day;
    int Month;
    int Year;
};
class Vector
{
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        cout << ' ' << x << ' ' << y << ' ' << z;
    }
    void Calc()
    {
        float a = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
        a = abs(a);
        cout << ' ' << a;
    }
private:
    double x;
    double y;
    double z;
};

int main()
{
    Calendar d(4, 9, 2020);
    d.Show();
    cout << endl;
    Vector v(1, 5, 9);
    v.Show();
    cout << endl;
    v.Calc();
    cout << endl << endl;


}